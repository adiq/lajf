# -*- encoding: utf8 -*-
import json
import subprocess
import time
import requests
from pprint import pprint

# Change to your VLC Path:
VLC_PATH = '/Applications/VLC.app/Contents/MacOS/VLC'
# TODO: autodetect vlc path based on OS

def run():
    data = getAppData()

    print("####################################################")
    print("    Lajw.py - update streamów: %s" % data['updatedAt'].encode('utf-8'))
    print("####################################################")

    # Load categories
    categories = {}
    for category in data['categories']:
        categories[category['id']] = category['name']

    # Load streams
    streams = []
    for stream in data['streams']:
        for i in stream['category']:
            tmp = dict(stream)
            tmp['category'] = i
            streams.append(tmp)

    category = showCategories(categories)
    stream = showStreams(streams, category)
    print("Uruchamiam %s..." % stream['name'])
    time.sleep(1)
    play(stream['type'], stream['url'])

def getAppData():
    json_data = requests.get('http://bitbucket.org/adiq/lajf/raw/master/source.json')
    data = json_data.json()
    return data


# Play router
def play(type, url):
    if type == "rtmp":
        playRtmp(url)

# Play RTMP
def playRtmp(url):
    execute(VLC_PATH +' -q --play-and-exit --no-play-and-exit '+url)

def execute(cmd):
    subprocess.call(cmd.split())

# Getting streams from specific category
def getStreams(streams, category=None):
    if category is None:
        raise Exception("ERROR: Invalid stream category")

    found = {}
    counter = 1
    for s in streams:
        if s['category'] == category:
            found[counter] = s
            counter += 1
    return found

def showCategories(categories):
    print("Wybierz kategorie:")
    for key, val in categories.iteritems():
        print("#%d | %s" % (key, val))
    inp = raw_input("Podaj numer kategorii: #")
    return int(inp)


def showStreams(streams, category):
    tmp = getStreams(streams, category)
    for key, val in tmp.iteritems():
        print("#%d | %s" % (key, val['name']))
    inp = raw_input("Podaj numer streamu: #")
    return tmp[int(inp)]


run()